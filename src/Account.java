import java.io.Serializable;
import java.util.Observable;

public abstract class  Account  extends Observable implements Serializable{


	protected int accountId;
	protected double amount;
	
	public Account(int amountId, double amount){
		this.accountId = accountId;
		this.amount = amount;
	}
	
	public void withdraw(double amount){};
	public void add(double amount){};
	

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	

	

}
