import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Serializable {

	public void serialize (Bank bank){
	      try {
	         FileOutputStream fileOut =
	         new FileOutputStream("CC:\\Users\\Mihai\\Desktop\\Tema_4");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(bank);
	         out.close();
	         fileOut.close();
	         System.out.printf("Serialized data is saved in /tmp/ser.");
	      }catch(IOException i) {
	         i.printStackTrace();
	      }
	   }
	
	public Bank restore()
	{
		Bank bank = null;
		try
		{
			FileInputStream fileIn = new FileInputStream("C:\\Users\\Mihai\\Desktop\\Tema_4");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bank= (Bank) in.readObject();
			in.close();
			fileIn.close();
		
		}
		catch(IOException i)
		{
			i.printStackTrace();
		}
		catch(ClassNotFoundException c)
		{
			System.out.println("Bank class not found");
			c.printStackTrace();
		}

		return bank;
	}
	
	}

