
public class SavingsAccount extends Account{

	
	public final static double rate = 0.5;
	public SavingsAccount(int accountId, double amount)
	{
	super(accountId, amount);
	}
	

	@Override
	public void withdraw(double amount) {
		double money = getAmount();
		double interest = money * rate;
		double total = interest + money;
		setAmount(0);
		notifyObservers(money);
		//extragem o suma mare de bani si setam 
		//contul pe 0, astfel ca in Saving s-o putem 
		//face doar o data
	}

	@Override
	public void add(double amount) {
		notifyObservers(amount);
	}
}
