import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


public class Bank implements BankProc {


	HashMap<Person, Set<Account>> hm = new HashMap<Person, Set<Account>>();
	ArrayList <Person> persons = new ArrayList<Person>();

	
	@Override
	public void add(Person p, int accountId, double price) {
		assert accountId >= 0 && p != null && price > 0;
		
		Set<Account> accounts;
		accounts = hm.get(p);
		double amount = 0;
	
		for(Account acc : accounts)
			if(acc.getAccountId() == accountId)
				{
				amount = acc.getAmount();
				amount = amount + price;
				acc.setAmount(amount);
				}
				
		assert accountId >= 0 && p != null && amount > price;

		
	}

	@Override
	public void withdraw(Person p, int accountId, double price) {
		assert accountId >= 0 && p != null && price > 0;
		
		Set<Account> accounts;
		accounts = hm.get(p);
		double amount = 0;
	
		for(Account acc : accounts)
			if(acc.getAccountId() == accountId)
				{
				amount = acc.getAmount();
				amount = amount - price;
				acc.setAmount(amount);
				}
				
		assert accountId >= 0 && p != null && amount < price;

		
	}

	@Override
	public void addPerson(Person p) {
		assert p!= null;
		
		hm.put(p, null);
		
		assert hm.get(p) != null;
		
	}

	@Override
	public void deletePerson(Person p) {
		assert p!= null;
		
		hm.remove(p);
		
		assert hm.remove(p) == null;
	}

	@Override
	public void addAccount(Account a, Person p) {
		
		assert a!= null && p != null && hm.size() > 0; 
		
		int size = hm.size();
		Set<Account> accounts;
		accounts = hm.get(p);
		accounts.add(a);
		
		hm.put(p, accounts);
		
		assert a != null && p != null && hm.size() == size + 1; 
	}

	@Override
	public void deleteAccount(Account a, Person p) {
		assert a != null && p != null && hm.size() > 0;
		int size = hm.size();
		
		Set<Account> accounts;
		accounts = hm.get(p);
		accounts.remove(a);
		
		hm.put(p, accounts);
		
		assert a != null && p != null && hm.size() == size + 1;

		
		
	}
	
	
}
