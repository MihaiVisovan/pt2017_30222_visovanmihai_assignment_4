
public interface BankProc {

	
	public void add(Person p, int accountId, double price);
	public void withdraw(Person p, int accountId , double price);
	public void addPerson(Person p);
	public void deletePerson(Person p);
	public void addAccount(Account a, Person p);
	public void deleteAccount(Account a, Person p);
}
