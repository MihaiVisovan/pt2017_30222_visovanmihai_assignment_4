
public class Person{


	String name;
	int personId;
	
	public Person()
	{
	
	}
	public Person(int personId, String name)
	{
		this.name = name;
		this.personId = personId;
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}
	
}
