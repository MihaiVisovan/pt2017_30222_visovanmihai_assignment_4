
public class SpendingsAccount extends Account {

	public SpendingsAccount(int accountId, double amount)
	
	{
	super(accountId, amount);	
		
	}

	@Override
	public void withdraw(double amount) {
		double money = getAmount();
		amount = amount - money;
		setAmount(amount);
		notifyObservers(amount);
	}

	@Override
	public void add(double amount) {
		double money = getAmount();
		amount = amount + money;
		setAmount(amount);
		notifyObservers(amount);
	}
}
